import java.util.ArrayList;

public class Node {
    int successor; //index in arraylist of its successor
    int depth; //part of rule which is evaluated
    String relation;
    ArrayList<Value> values = new ArrayList<Value>();
}
