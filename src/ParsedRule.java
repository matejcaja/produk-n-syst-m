import java.util.ArrayList;

public class ParsedRule {
    public ArrayList<ParsedFact> leftSide = new ArrayList<ParsedFact>();
    public ArrayList<ParsedFact> rightSide = new ArrayList<ParsedFact>();
    private String ruleName;

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }
}
