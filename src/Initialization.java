import javax.swing.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Set;

public class Initialization {


    public NodeList xmlOpenRules(){
        URL url = getClass().getResource("rules.xml");
        File file = new File(url.getPath());

        NodeList nList = null;

        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            nList = doc.getElementsByTagName("rule");
        }catch(Exception e){
            e.getMessage();
        }


        return nList;
    }

    public NodeList xmlOpenFacts(){
        URL url = getClass().getResource("facts.xml");
        File file = new File(url.getPath());

        NodeList nList = null;

        try{
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            nList = doc.getElementsByTagName("fact");
        }catch(Exception e){
            e.getMessage();
        }


        return nList;
    }

    public String[] loadFacts(JTextArea textAreaFacts, int FactsSize){
        String[] facts = new String[FactsSize];
        NodeList nList = xmlOpenFacts();

        for(int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);

            Element e = (Element) nNode;
            facts[i] = e.getAttribute("value");

            textAreaFacts.append(e.getAttribute("value") +"\n");
        }

        return facts;
    }

    public Hashtable loadRules(JTextArea textAreaRules){
        Hashtable rules = new Hashtable();
        NodeList nList = xmlOpenRules();

        for(int i = 0; i < nList.getLength(); i++){
            Node nNode = nList.item(i);

            Element e = (Element) nNode;
            rules.put(e.getAttribute("name"), e.getElementsByTagName("value").item(0).getTextContent());
            textAreaRules.append(e.getAttribute("name")+"\n"+e.getElementsByTagName("value").item(0).getTextContent() +"\n\n");
        }
        return rules;
    }

    public void parseFacts(String[] facts, ArrayList parsedFacts){
        //for every fact in String array of facts
        for(int i = 0; i < facts.length; i++){
            ParsedFact Fact = new ParsedFact();
            //split it
            String[] split = facts[i].split(" ");
            //for every splited String, find if it is a name of relation and save it
            for(int j = 0; j < split.length; j++){
                if(Character.isUpperCase(split[j].charAt(0))){
                    if(Fact.getName1() == null){
                        Fact.setName1(split[j]);
                    }else{
                        Fact.setName2(split[j]);
                    }
                }else{
                    Fact.setRelation(split[j]);
                }
            }

          parsedFacts.add(Fact);
        }
    }

    public void parseRules(Hashtable rules, ArrayList parsedRules){
        Set<String> keys = rules.keySet();


        //every rule from rules
        for(String key : keys){
            ParsedRule Rule = new ParsedRule();

            //save ruleName just in case somebody would ask :D
            Rule.setRuleName(key);
            //get lef and right side of the rule 0 - left, 1 - right
            String[] leftAndRight = rules.get(key).toString().split("POTOM");
            //get all rules from left side, ommit first 3
            String[] leftRules = leftAndRight[0].substring(3).split(",");
            //handle left side of the rule
            handleLeftSideOfTheRule(leftRules, Rule.leftSide);

            String[] rightRules = leftAndRight[1].split(",");
            //handle right side of the rule
            handleRightSideOfTheRule(rightRules, Rule.rightSide);

            parsedRules.add(Rule);
        }
    }

    public void handleLeftSideOfTheRule(String[] leftRules, ArrayList leftSide){


        for(int i = 0; i < leftRules.length; i++){
            ParsedFact Fact = new ParsedFact();

            String[] value = leftRules[i].trim().split(" ");
            for(int j = 0; j < value.length; j++){
                if(value[j].trim().charAt(0) == '?'){
                    if(Fact.getName1() == null){
                        Fact.setName1(value[j]);
                    }else{
                        Fact.setName2(value[j]);
                    }
                }
                else{
                    Fact.setRelation(value[j]);
                }
            }

            leftSide.add(Fact);
        }
    }

    public void handleRightSideOfTheRule(String[] rightRules, ArrayList rightSide){
        for(int i = 0; i < rightRules.length; i++){
            ParsedFact Fact = new ParsedFact();

            String[] value = rightRules[i].trim().split(" ");
            for (int j = 0; j < value.length; j++) {
                //check right side action, hard coded
                if(checkAction(value[j].trim())){
                    Fact.setAction(value[j].trim());
                } else {
                    if (value[j].trim().charAt(0) == '?') {
                        if (Fact.getName1() == null) {
                            Fact.setName1(value[j]);
                        } else {
                            Fact.setName2(value[j]);
                        }
                    } else {
                        Fact.setRelation(value[j]);
                    }
                }
            }

            rightSide.add(Fact);
        }
    }

    public Boolean checkAction(String value){
        String[] actions = {"pridaj",
                            "vymaz",
                            "sprava"
                            };

        for(int i = 0; i < actions.length; i++){
            if(value.equals(actions[i])){
                return true;
            }
        }

        return false;

    }

}
