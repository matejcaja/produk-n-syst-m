import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Hashtable;

public class GUI extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextArea textAreaRules;
    private JTextArea textAreaMemory;
    private JTextArea textAreaTemp;
    private JTextArea textAreaOut;
    private JButton initButton;
    private Initialization init = new Initialization();

    //order of execution
    private String[] executePosition = {"DruhyRodic1","DruhyRodic2","Otec","Matka","Surodenci","Brat","Stryko","Test mazania"};
    private int FactsSize = 12;
    String[] facts = new String[FactsSize];
    Hashtable rules = new Hashtable();
    Runner runner = new Runner();
    ArrayList<ParsedFact> parsedFacts = new ArrayList<ParsedFact>();
    ArrayList<ParsedRule> parsedRules = new ArrayList<ParsedRule>();
    Hashtable RulesWithOrder = new Hashtable();
    private int step = 0;

    public GUI() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.setEnabled(false);

        textAreaMemory.setEditable(false);
        textAreaOut.setEditable(false);
        textAreaRules.setEditable(false);
        textAreaTemp.setEditable(false);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        initButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onInit();
            }
        });
    }

    public static void main(String[] args) {
        GUI dialog = new GUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    private void onOK() {
        if(step > 7) step = 0;

        runner.run(parsedFacts, (ParsedRule) RulesWithOrder.get(executePosition[step]), textAreaTemp);
        updateFacts();

        //move to another rule
        step++;
    }

    private void onCancel() {
        //runner.run(parsedFacts, RulesWithOrder, executePosition);
        dispose();
    }

    private void onInit() {
        textAreaRules.setText("");
        textAreaMemory.setText("");
        rules.clear();
        parsedFacts.clear();
        parsedRules.clear();

        rules = init.loadRules(textAreaMemory);
        facts = init.loadFacts(textAreaRules, FactsSize);
        init.parseFacts(facts, parsedFacts);
        init.parseRules(rules, parsedRules);

        buttonOK.setEnabled(true);

        for(int i = 0; i < parsedRules.size(); i++){
            /*  just in case, make sure rules will be execut  
                in order of xml file, not like from iterator
             */
            RulesWithOrder.put(parsedRules.get(i).getRuleName(), parsedRules.get(i));
        }
    }

    public void updateFacts(){
        textAreaRules.setText("");
        for(int i = 0; i < parsedFacts.size(); i++){
            textAreaRules.append(parsedFacts.get(i).getName1() + " " +parsedFacts.get(i).getRelation() + " " + parsedFacts.get(i).getName2() +"\n");
        }
    }
}
