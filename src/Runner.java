import javax.swing.*;
import java.util.ArrayList;

public class Runner {
    public void run(ArrayList<ParsedFact> parsedFacts, ParsedRule Rule, JTextArea textAreaTemp){
        ArrayList<Node> tree = new ArrayList<Node>();
        int maxDepth = Rule.leftSide.size()-1;

        //first initialization with rule 1
        String relationRule = Rule.leftSide.get(0).getRelation();
        for (int j = 0; j < parsedFacts.size(); j++) {
            String relationFact = parsedFacts.get(j).getRelation();
            if (relationRule.equals(relationFact)) {
                Node node = new Node();
                Value value1 = new Value(Rule.leftSide.get(0).getName1(), parsedFacts.get(j).getName1());
                Value value2 = new Value(Rule.leftSide.get(0).getName2(), parsedFacts.get(j).getName2());

                node.values.add(value1);
                node.values.add(value2);
                node.relation = relationFact;
                node.successor = -1;
                node.depth = 0;

                tree.add(node);//add result to tree
            }
        }

        //continue with other rules
        int treeCnt = 0;
        for(int ruleCnt = 1; ruleCnt < Rule.leftSide.size(); ruleCnt++){
            relationRule = Rule.leftSide.get(ruleCnt).getRelation();
            //if all nodes from current depth are computed, continue with new rule and new depth
            while(treeCnt < tree.size() && tree.get(treeCnt).depth < ruleCnt){
                //for every node, find its facts
                for(int factCnt = 0; factCnt < parsedFacts.size(); factCnt++){
                    String relationFact = parsedFacts.get(factCnt).getRelation();
                    if(relationRule.equals("<>")){
                        //odtialto

                        for(int valueCnt = 0; valueCnt < tree.get(treeCnt).values.size(); valueCnt++){

                            if(tree.get(treeCnt).values.get(valueCnt).tag.equals(Rule.leftSide.get(ruleCnt).getName1())){
                                //if matchig tags are found, check them if they have the same value in name
                                if(tree.get(treeCnt).values.get(valueCnt).name.equals(parsedFacts.get(factCnt).getName1())){
                                    Node node = new Node();

                                    Value value2 = new Value(Rule.leftSide.get(ruleCnt).getName2(), parsedFacts.get(factCnt).getName2());
                                    for(int i = 0; i < tree.get(treeCnt).values.size(); i++){
                                        node.values.add(new Value(tree.get(treeCnt).values.get(i).tag,tree.get(treeCnt).values.get(i).name));
                                    }
                                    node.values.add(value2);
                                    node.relation = relationFact;
                                    node.depth = ruleCnt;
                                    node.successor = treeCnt;

                                    tree.add(node);
                                }
                            }else if(tree.get(treeCnt).values.get(valueCnt).tag.equals(Rule.leftSide.get(ruleCnt).getName2())){
                                if(tree.get(treeCnt).values.get(valueCnt).name.equals(parsedFacts.get(factCnt).getName2()) && parsedFacts.get(factCnt).getName2() != null){
                                    Node node = new Node();

                                    Value value2 = new Value(Rule.leftSide.get(ruleCnt).getName1(), parsedFacts.get(factCnt).getName1());

                                    for(int i = 0; i < tree.get(treeCnt).values.size(); i++){
                                        node.values.add(new Value(tree.get(treeCnt).values.get(i).tag,tree.get(treeCnt).values.get(i).name));
                                    }
                                    node.values.add(value2);
                                    node.relation = relationFact;
                                    node.depth = ruleCnt;
                                    node.successor = treeCnt;

                                    tree.add(node);

                                }
                            }
                        }
                        //sem
                    }else{
                    if (relationRule.equals(relationFact)) {
                        //for every value int node from tree, try to match tags
                        for(int valueCnt = 0; valueCnt < tree.get(treeCnt).values.size(); valueCnt++){

                            if(tree.get(treeCnt).values.get(valueCnt).tag.equals(Rule.leftSide.get(ruleCnt).getName1())){
                                //if matchig tags are found, check them if they have the same value in name
                                if(tree.get(treeCnt).values.get(valueCnt).name.equals(parsedFacts.get(factCnt).getName1())){
                                    Node node = new Node();

                                    Value value2 = new Value(Rule.leftSide.get(ruleCnt).getName2(), parsedFacts.get(factCnt).getName2());
                                    for(int i = 0; i < tree.get(treeCnt).values.size(); i++){
                                        node.values.add(new Value(tree.get(treeCnt).values.get(i).tag,tree.get(treeCnt).values.get(i).name));
                                    }
                                    node.values.add(value2);
                                    node.relation = relationFact;
                                    node.depth = ruleCnt;
                                    node.successor = treeCnt;

                                    tree.add(node);
                                }
                            }else if(tree.get(treeCnt).values.get(valueCnt).tag.equals(Rule.leftSide.get(ruleCnt).getName2())){
                                if(tree.get(treeCnt).values.get(valueCnt).name.equals(parsedFacts.get(factCnt).getName2()) && parsedFacts.get(factCnt).getName2() != null){
                                    Node node = new Node();

                                    Value value2 = new Value(Rule.leftSide.get(ruleCnt).getName1(), parsedFacts.get(factCnt).getName1());

                                    for(int i = 0; i < tree.get(treeCnt).values.size(); i++){
                                        node.values.add(new Value(tree.get(treeCnt).values.get(i).tag,tree.get(treeCnt).values.get(i).name));
                                    }
                                    node.values.add(value2);
                                    node.relation = relationFact;
                                    node.depth = ruleCnt;
                                    node.successor = treeCnt;

                                    tree.add(node);

                                }
                            }
                        }

                    }
                }
                }
                treeCnt++;
            }
        }
        int position = -1;
        int maxLast = 0;

        for(int i = 0; i < tree.size(); i++){
            if(tree.get(i).depth == maxDepth){
                maxLast++;
            }
        }

        position = getPosition(tree, maxDepth, position);

        for(int i = 0; i < tree.size(); i++){
            if(tree.get(i).depth == maxDepth){
                position = i;
                break;
            }
        }
        if (tree.size() > 0) {
            System.out.println(tree.get(position).relation);
            for (int j = 0; j < tree.get(position).values.size(); j++) {
                System.out.println(tree.get(position).values.get(j).name + " " + tree.get(position).values.get(j).tag);
            }
            System.out.println();

            for (int i = 0; i < Rule.rightSide.size(); i++) {
                if (Rule.rightSide.get(i).getAction().equals("pridaj")) {

                    //int a = addFact(parsedFacts, tree.get(position), Rule.rightSide.get(i));
                    for(int s = 0; s <= maxLast; s++){
                        position = getPosition(tree,maxDepth,position);
                        int a = addFact(parsedFacts, tree.get(position), Rule.rightSide.get(i));
                        if(a == 0)break;
                    }


                }
                if (Rule.rightSide.get(i).getAction().equals("sprava")) {
                    addMessage(tree.get(position), textAreaTemp, Rule.rightSide.get(i));
                }
            }
        }
    }

    public int addFact(ArrayList<ParsedFact> parsedFacts, Node node, ParsedFact Rule){
        ParsedFact newFact = new ParsedFact();

        newFact.setRelation(Rule.getRelation());

        for(int i = 0; i < node.values.size(); i++){
            if(Rule.getName1().equals(node.values.get(i).tag)){
                 newFact.setName1(node.values.get(i).name);
            }else if(Rule.getName2().equals(node.values.get(i).tag)){
                newFact.setName2(node.values.get(i).name);
            }
        }

        int check = 0;
        for(int i = 0; i < parsedFacts.size(); i++){
            if(parsedFacts.get(i).getName1().equals(newFact.getName1()) && parsedFacts.get(i).getName2().equals(newFact.getName2()) && parsedFacts.get(i).getRelation().equals(newFact.getRelation())){
                check++;
            }
        }
        if(check == 0){
            parsedFacts.add(newFact);
        }

        return check;
    }

    public void addMessage(Node node, JTextArea textArea, ParsedFact Rule){
        textArea.append(Rule.getName1());

        for(int i = 0; i < node.values.size();i++){
            if(node.values.get(i).tag.equals(Rule.getName1())){
                textArea.append(node.values.get(i).name + " ");
            }
        }

        textArea.append(node.relation +"\n");
    }

    public int getPosition(ArrayList<Node> tree, int maxDepth, int position){
        if(position != -1){
            for(int i = 0; i < tree.size(); i++){
                if(tree.get(i).depth == maxDepth && position != i){
                    position = i;
                    break;
                }
            }
        }else if(position == -1){
            for(int i = 0; i < tree.size(); i++){
                if(tree.get(i).depth == maxDepth){
                    position = i;
                    break;
                }
            }
        }


        return position;
    }

    /*
    public void run(ArrayList<ParsedFact> parsedFacts, ParsedRule Rule, JTextArea textAreaTemp){
        Hashtable premenne = new Hashtable();
        int breaker = 0;

        //to recover if there was a bad rule
        Integer[] dimensions = new Integer[Rule.leftSide.size()];
        for(int i = 0; i < dimensions.length; i++) dimensions[i] = 0;

        //all rules
        for(int i = 0; i < Rule.leftSide.size(); i++){
            String relationRule = Rule.leftSide.get(i).getRelation();
            int j = 0;
            while(true){
                if(j >= parsedFacts.size()){
                    j = 0;
                    dimensions[i--]++;
                    i = 0;
                    premenne.clear();

                    if(breaker == 10)break;
                    breaker++;
                }

                String relationFact = parsedFacts.get(j).getRelation();
                if(relationFact.equals(relationRule)){
                    dimensions[i] = j;
                    if( !premenne.containsKey(Rule.leftSide.get(i).getName1()) && !premenne.containsKey(Rule.leftSide.get(i).getName2())){
                        premenne.put(Rule.leftSide.get(i).getName1(), parsedFacts.get(j).getName1());
                        premenne.put(Rule.leftSide.get(i).getName2(), parsedFacts.get(j).getName2());
                        break;
                    }else{
                            //if (premenne.containsKey(Rule.leftSide.get(i).getName1()) && !premenne.containsKey(Rule.leftSide.get(i).getName2())) {
                        if(premenne.containsKey(Rule.leftSide.get(i).getName1()) && premenne.get(Rule.leftSide.get(i).getName1()).equals(parsedFacts.get(j).getName1())){
                                premenne.put(Rule.leftSide.get(i).getName2(), parsedFacts.get(j).getName2());
                                break;

                        }//else if(premenne.containsKey(Rule.leftSide.get(i).getName2()) && !premenne.containsKey(Rule.leftSide.get(i).getName1())){
                            else if(premenne.containsKey(Rule.leftSide.get(i).getName2()) && premenne.get(Rule.leftSide.get(i).getName2()).equals(parsedFacts.get(j).getName2())){
                                premenne.put(Rule.leftSide.get(i).getName1(), parsedFacts.get(j).getName1());
                                break;
                            }
                        }

                }else{
                    j++;
                }

            }
        }

        premenne.size();

        for(int i = 0; i < Rule.rightSide.size(); i++){
            if(Rule.rightSide.get(i).getAction().equals("pridaj")){
                pridaj(parsedFacts,premenne, Rule.rightSide.get(i));
            }else if(Rule.rightSide.get(i).getAction().equals("vymaz")){
                vymaz(parsedFacts);
            }else if(Rule.rightSide.get(i).getAction().equals("sprava")){
                sprava(Rule.rightSide.get(i),textAreaTemp, premenne);
            }
        }
    }

    public void pridaj(ArrayList<ParsedFact> parsedFacts, Hashtable premenne, ParsedFact Rule){
        ParsedFact newFact = new ParsedFact();
        newFact.setName1((String)premenne.get(Rule.getName1()));
        newFact.setName2((String) premenne.get(Rule.getName2()));
        newFact.setRelation(Rule.getRelation());

        parsedFacts.add(newFact);

    }

    public void vymaz(ArrayList<ParsedFact> parsedFacts){

    }

    public void sprava(ParsedFact Fact, JTextArea textAreaTemp, Hashtable premenne){

    }
*/

}
